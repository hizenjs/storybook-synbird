import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import tsconfigPaths from 'vite-tsconfig-paths'
import plainText from 'vite-plugin-plain-text'

export default defineConfig({
  plugins: [
    react(),
    tsconfigPaths(),
    plainText([/\/LICENSE$/, '**/*.vtt', /\.glsl$/], { namedExport: false })
  ],
  define: {
    'process.env': process.env,
  },
  publicDir: 'public',
})
