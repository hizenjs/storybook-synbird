const Default = `
  * {
    font-family: 'Inter', sans-serif;
  }
  body: {
    background-color: #FFFFFF;
    color: #212121;
    font-size: 16px;
    max-width: 100%;
    overflow-x: hidden;
  }
`

export default Default