export const size = {
  mobileS: 375,
  mobileM: 590,
  mobileL: 774,
  tablet: 992,
  laptop: 1024,
  laptopL: 1440,
  desktop: 2560,
}

export const device = {
  mobileS: `(max-width: ${size.mobileS}px)`,
  mobileM: `(max-width: ${size.mobileM}px)`,
  mobileL: `(max-width: ${size.mobileL}px)`,
  tablet: `(max-width: ${size.tablet}px)`,
  laptop: `(max-width: ${size.laptop}px)`,
  laptopL: `(max-width: ${size.laptopL}px)`,
  desktop: `(max-width: ${size.desktop}px)`,
}
