
type OpacityValues = 100 | 90 | 80 | 70 | 60 | 50 | 40 | 30 | 20 | 10

const black = `33,33,33`
const white = `255,255,255`

export const base = {
  transition: {
    fast: '200ms',
    normal: '300ms',
    slow: '1s',
    custom: (ms: number) => `${ms}ms`
  },
  hover: (color: string) => `${color}D1`
}

export const light = {
  ...base,
  colors: {
    default: (value: OpacityValues = 100) => `rgba(${black},${value/100})`,
    invert: (value: OpacityValues = 100) => `rgba(${white},${value/100})`,
    primary: '#7D4496',
    secondary: '#1E1A45',
    info: '#2F96B4',
    success: '#53AB6B',
    warning: '#F9A919',
    danger: '#EA3343',
  }
}

export const dark = {
  ...base,
  colors: {
    default: (value: OpacityValues = 100) => `rgba(${white},${value/100})`,
    invert: (value: OpacityValues = 100) => `rgba(${black},${value/100})`,
    primary: '#7D4496',
    secondary: '#1E1A45',
    info: '#2F96B4',
    success: '#53AB6B',
    warning: '#F9A919',
    danger: '#EA3343',
  },
}