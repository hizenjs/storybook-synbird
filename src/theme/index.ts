import baseStyled, {
  createGlobalStyle,
  keyframes,
  css,
  ThemedStyledInterface,
} from 'styled-components'
import Default from './default'
import { light, dark } from './newTheme'

const GlobalStyle = createGlobalStyle`${Default}`

const asRGB = (hex: string, alpha: number = 1) => {
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    let c: string | string[] = hex.substring(1).split('')
    if (c.length == 3) c = [c[0], c[0], c[1], c[1], c[2], c[2]]
    c = '0x' + c.join('')
    return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${alpha})`
  }
  throw new Error('Bad Hex')
}

export default GlobalStyle
export { keyframes, css, asRGB }
export { dark, light }
export { size, device } from './breakpoints'
export type Theme = typeof light
export const styled = baseStyled as ThemedStyledInterface<Theme>
