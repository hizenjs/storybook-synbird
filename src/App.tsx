import Button from 'components/button/Button'
import { ReactElement } from 'react'
import Alert from './components/alert/Alert'

export default function App(): ReactElement {
  return (
    <div>
      <div style={{ display: 'flex', alignItems: 'end', margin: 10 }}>
        <Button
          onClick={() => alert('Hello')}
          variant="outlined"
          size="lg"
          color="info"
          disabled={false}
          icon={{ position: 'left', name: 'user' }}
        >
          Squared
        </Button>
      </div>
      <div style={{ display: 'flex', alignItems: 'end', margin: 10 }}>
        <Button
          onClick={() => alert('Hello')}
          variant="squared"
          size="lg"
          color="info"
          disabled={false}
          icon={{ position: 'left', name: 'user' }}
        />
      </div>
      <Alert
        text={'Detailed description and advice about successful copyrighting.'}
        type={'info'}
        title={'Success Tips'}
        icon
        closable
        margin={10}
      />
      <Alert
        text={'Detailed description and advice about successful copyrighting.'}
        type={'success'}
        icon={false}
        margin={10}
      />
      <Alert
        text={'Detailed description and advice about successful copyrighting.'}
        type={'warning'}
        title={'Success Tips'}
        icon={false}
        closable
        margin={10}
      />
      <Alert
        text={'Detailed description and advice about successful copyrighting.'}
        type={'danger'}
        title={'Success Tips'}
        icon
        closable
        margin={10}
      />
    </div>
  )
}
