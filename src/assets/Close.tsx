import { IconProps } from 'components/icon/Icon'

export default function Accessibility({ size, color }: IconProps) {
  return (
    <svg
      style={{ transform: `scale(${size || 1})` }}
      width="25"
      height="25"
      viewBox="0 0 25 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M19.5 6.5477L18.09 5.1377L12.5 10.7277L6.91 5.1377L5.5 6.5477L11.09 12.1377L5.5 17.7277L6.91 19.1377L12.5 13.5477L18.09 19.1377L19.5 17.7277L13.91 12.1377L19.5 6.5477Z"
        fill={color || '#212121'}
      />
    </svg>
  )
}
