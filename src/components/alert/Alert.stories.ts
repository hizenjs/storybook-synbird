import type { Meta, StoryObj } from '@storybook/react'
import Alert, { AlertProps } from './Alert'

const meta: Meta<typeof Alert> = {
  title: 'Components/Alert',
  component: Alert,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof meta>

export const Default: { args: AlertProps } = {
  args: {
    text: 'Detailed description and advice about successful copyrighting.',
    type: 'info',
    title: 'Information Tips',
    closable: true,
  },
}
