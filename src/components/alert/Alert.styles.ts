import { styled, asRGB } from 'theme'
import { Type } from './Alert'

interface IStyle {
  type: Type
  maxWidth?: number | undefined
  margin?: number | number[] | undefined
}

const Wrapper = styled.div<IStyle>`
  position: relative;
  display: flex;
  padding: 8px 10px;
  border-radius: 5px;
  border: 1px solid ${({ theme, type }) => theme.colors[type || 'info']};
  background: ${({ theme, type }) => asRGB(theme.colors[type || 'info'], 0.6)};
  max-width: ${({ maxWidth }) => (maxWidth ? `${maxWidth}px` : 'fit-content')};
  box-shadow: 0 6px 12px rgba(0, 0, 0, 0.18);
  margin: ${({ margin }) =>
    margin ? (Array.isArray(margin) ? `${margin.join('px ')}px` : `${margin}px`) : 'unset'};
`

const Col = styled.div`
  display: flex;
  flex-direction: column;
`

const Text = styled.p`
  margin: 0;
  font-family: 'Inter', sans-serif;
  font-size: 12px;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.default()};
  cursor: default;
`

const Title = styled.h4`
  font-family: 'Inter', sans-serif;
  font-size: 12px;
  font-weight: 600;
  margin: 0 0 8px 0;
  color: ${({ theme }) => theme.colors.default(80)};
  cursor: default;
`

const Close = styled.button`
  z-index: 1;
  display: flex;
  position: absolute;
  right: 3px;
  top: 1px;
  background: transparent;
  padding: 0;
  border: none;
  cursor: pointer;
  transform: scale(0.5) !important;
  transition: ${({ theme }) => theme.transition.normal};
  &:hover {
    transform: scale(0.5) rotate(90deg) !important;
  }
`

const IconWrapper = styled.div<IStyle>`
  display: flex;
  width: 14px;
  height: 14px;
  border-radius: 20px;
  justify-content: center;
  align-items: center;
  gap: 10px;
  flex-shrink: 0;
  padding: 2px;
  background: ${({ theme, type }) => theme.colors[type || 'info']};
  margin-right: 8px;
  & path {
    fill: ${({ theme }) => theme.colors.invert()};
  }
`

const styles = {
  Wrapper,
  Col,
  Text,
  Title,
  Close,
  IconWrapper,
}

export default styles
