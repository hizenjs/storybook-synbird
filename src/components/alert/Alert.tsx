import { ReactElement, MutableRefObject, CSSProperties, useRef } from 'react'
import styles from './Alert.styles'
import Icon from '../icon/Icon'

export type Type = 'info' | 'success' | 'warning' | 'danger'

export type AlertProps = {
  title?: string
  text: string
  type: Type
  maxWidth?: number
  icon?: boolean
  closable?: boolean
  margin?: number | number[]
  style?: CSSProperties | undefined
}

const { Wrapper, Col, IconWrapper, Title, Text, Close } = styles

export default function Alert({
  title,
  text,
  type,
  maxWidth,
  style,
  margin,
  icon = true,
  closable = false,
}: AlertProps): ReactElement {
  const ref = useRef() as MutableRefObject<HTMLInputElement>

  const handleClose = () => ref.current && ref.current?.parentNode?.removeChild(ref.current)

  return (
    <Wrapper ref={ref} type={type} maxWidth={maxWidth} style={style} margin={margin}>
      <Col>
        {icon && (
          <IconWrapper type={type}>
            <Icon name="close" />
          </IconWrapper>
        )}
      </Col>
      <Col>
        {title && <Title>{title}</Title>}
        <Text>{text}</Text>
      </Col>
      {closable && (
        <Close onClick={handleClose}>
          <Icon name="close" />
        </Close>
      )}
    </Wrapper>
  )
}
