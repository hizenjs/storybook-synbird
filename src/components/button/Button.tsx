import { ReactElement } from 'react'
import { Styled } from './Button.styles'
import Icon from '../icon/Icon'

type Colors = 'primary' | 'secondary' | 'info' | 'success' | 'warning' | 'danger'
type Variants = 'rounded' | 'squared' | 'outlined' | 'transparent'
type Sizes = 'sm' | 'md' | 'lg'

export type ButtonProps = {
  style?: Record<string, string | number>
  children?: string | number
  disabled?: boolean
  color?: Colors
  variant?: Variants
  size?: Sizes
  icon?: { position?: 'left' | 'right'; name: string }
  onClick?: () => void
}

export default function Button({
  onClick,
  style,
  children,
  variant,
  disabled,
  color,
  size,
  icon,
}: ButtonProps): ReactElement {
  const buttonProps = { variant, disabled, color, size, icon, position: icon?.position }
  const handleClick = () => onClick && onClick()

  switch (variant) {
    case 'transparent':
      return (
        <Styled.Transparent
          style={style}
          onClick={handleClick}
          hasChildren={!!children}
          {...buttonProps}
        >
          {icon?.name && <Icon name={icon?.name} />}
          {children && <span>{children}</span>}
        </Styled.Transparent>
      )
    case 'rounded':
      return (
        <Styled.Rounded
          style={style}
          onClick={handleClick}
          hasChildren={!!children}
          {...buttonProps}
        >
          {icon?.name && <Icon name={icon?.name} />}
          {children && <span>{children}</span>}
        </Styled.Rounded>
      )
    case 'outlined':
      return (
        <Styled.Outlined
          style={style}
          onClick={handleClick}
          hasChildren={!!children}
          {...buttonProps}
        >
          {icon?.name && <Icon name={icon?.name} />}
          {children && <span>{children}</span>}
        </Styled.Outlined>
      )
  }
  return (
    <Styled.Squared style={style} onClick={handleClick} hasChildren={!!children} {...buttonProps}>
      {icon?.name && <Icon name={icon?.name} />}
      {children && <span>{children}</span>}
    </Styled.Squared>
  )
}
