import type { Meta, StoryObj } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Button, { ButtonProps } from './Button'

const meta: Meta<typeof Button> = {
  title: 'Components/Button',
  component: Button,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
}

export default meta

type Story = StoryObj<typeof meta>

export const Default: { args: ButtonProps } = {
  args: {
    disabled: false,
    color: 'primary',
    variant: 'outlined',
    size: 'lg',
    icon: {
      name: 'accessibility',
      position: 'left',
    },
    onClick: action('clicked'),
    children: 'Accessibility',
  },
}
