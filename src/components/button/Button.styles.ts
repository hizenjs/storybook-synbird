import { styled, css } from 'theme'
import { ButtonProps } from './Button'

type ButtonType = Omit<ButtonProps, 'children' | 'onClick' | 'style' | 'icon'>

interface IButton extends ButtonType {
  hasChildren: boolean
  position?: 'left' | 'right'
}

const padding = {
  sm: css<IButton>`
    padding: 4px ${({ hasChildren }) => (hasChildren ? '12px' : '')};
  `,
  md: css<IButton>`
    padding: 8px ${({ hasChildren }) => (hasChildren ? '22px' : '')};
  `,
  lg: css<IButton>`
    padding: 12px ${({ hasChildren }) => (hasChildren ? '26px' : '')};
  `,
}

const boxShadox = css<IButton>`
  box-shadow: 0 2px 8px ${({ theme, color }) => `${theme.colors[color || 'primary']}80`};
`

const Button = styled.button<IButton>`
  flex-shrink: 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: ${({ position }) => (position === 'right' ? 'row-reverse' : 'row')};
  ${({ size }) => (size === 'sm' ? padding.sm : size === 'lg' ? padding.lg : padding.md)};
  border: ${({ size }) => (size === 'lg' ? 2 : 1)}px solid;
  font-weight: ${({ size }) => (size === 'sm' ? 300 : size === 'lg' ? 600 : 400)};
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
  transition: ${({ theme }) => theme.transition.normal};
  font-family: 'Inter', sans-serif;
  font-size: 16px;
  & svg {
    ${({ position, hasChildren }) =>
      hasChildren ? (position === 'right' ? `margin-left: 10px` : `margin-right: 10px`) : ''};
  }
  & path {
    transition: ${({ theme }) => theme.transition.normal};
  }
`

export const Styled = {
  Rounded: styled(Button)<IButton>`
    border: none;
    border-radius: 46px;
    color: ${({ theme }) => theme.colors.invert()};
    background: ${({ theme, color, disabled }) =>
      disabled ? theme.colors.default(20) : theme.colors[color || 'primary']};
    border-color: ${({ theme, color, disabled }) =>
      disabled ? theme.colors.default(20) : theme.colors[color || 'primary']};
    & path {
      fill: ${({ theme }) => theme.colors.invert()};
    }
    &:hover {
      background: ${({ theme, color, disabled }) =>
        !disabled && theme.hover(theme.colors[color || 'primary'])};
      color: ${({ theme, disabled }) => !disabled && theme.colors.invert(100)};
      ${({ disabled }) => !disabled && boxShadox}
    }
  `,
  Squared: styled(Button)<IButton>`
    border: none;
    border-radius: 5px;
    color: ${({ theme }) => theme.colors.invert()};
    background: ${({ theme, color, disabled }) =>
      disabled ? theme.colors.default(20) : theme.colors[color || 'primary']};
    & path {
      fill: ${({ theme }) => theme.colors.invert()};
    }
    &:hover {
      background: ${({ theme, color, disabled }) =>
        !disabled && theme.hover(theme.colors[color || 'primary'])};
      color: ${({ theme, disabled }) => !disabled && theme.colors.invert(100)};
      ${({ disabled }) => !disabled && boxShadox}
    }
  `,
  Outlined: styled(Button)<IButton>`
    border-radius: 5px;
    background: transparent;
    color: ${({ theme, color, disabled }) =>
      disabled ? theme.colors.default(20) : theme.colors[color || 'primary']};
    border-color: ${({ theme, color, disabled }) =>
      disabled ? theme.colors.default(20) : theme.colors[color || 'primary']};
    & path {
      fill: ${({ theme, color, disabled }) =>
        disabled ? theme.colors.default(20) : theme.colors[color || 'primary']};
    }
    &:hover {
      background: ${({ theme, color, disabled }) =>
        !disabled && theme.hover(theme.colors[color || 'primary'])};
      color: ${({ theme, disabled }) => !disabled && theme.colors.invert(100)};
      ${({ disabled }) => !disabled && boxShadox}
      & path {
        fill: ${({ theme, disabled }) => !disabled && theme.colors.invert(100)};
      }
    }
  `,
  Transparent: styled(Button)<IButton>`
    border: none;
    background: transparent;
    color: ${({ theme, color, disabled }) =>
      disabled ? theme.colors.default(20) : theme.colors[color || 'primary']};
    & path {
      fill: ${({ theme, color, disabled }) =>
        disabled ? theme.colors.default(20) : theme.colors[color || 'primary']};
    }
    &:hover {
      transform: scale(1.04);
      color: ${({ theme, disabled, color }) =>
        !disabled && theme.hover(theme.colors[color || 'primary'])};
    }
  `,
}
