import type { Meta, StoryObj } from '@storybook/react'
import Icon, { IconProps } from './Icon'

const meta: Meta<typeof Icon> = {
  title: 'Components/Icon',
  component: Icon,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof meta>

export const Default: { args: IconProps } = {
  args: {
    color: '#212121',
    size: 4,
    name: 'accessibility',
  },
}
