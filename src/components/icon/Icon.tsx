import { ReactElement } from 'react'
import { Accessibility, Close } from 'assets'

type Colors = 'primary' | 'secondary' | 'info' | 'success' | 'warning' | 'danger'

export type IconProps = {
  size?: number
  color?: Colors | string
  name?: string
}

const Fallback = (props: IconProps) => <Accessibility {...props} />

export default function Icon(props: IconProps): ReactElement {
  switch (props.name) {
    case 'accessibility':
      return <Accessibility {...props} />
    case 'close':
      return <Close {...props} />
  }
  return <Fallback {...props} />
}
