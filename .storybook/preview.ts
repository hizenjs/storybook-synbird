import type { Preview } from "@storybook/react";
import { withThemeFromJSXProvider } from '@storybook/addon-themes';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import { dark, light } from '../src/theme'
const GlobalStyles = createGlobalStyle`
  * {
    font-family: 'Inter', sans-serif;
  }
  body: {
    background-color: #FFFFFF;
    color: #212121;
    font-size: 16px;
    max-width: 100%;
    overflow-x: hidden;
  }
`;

export const decorators = [
  withThemeFromJSXProvider({
  themes: {
    light: light,
    dark: dark
  },
  defaultTheme: 'light',
  Provider: ThemeProvider,
  GlobalStyles,
})];

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
};

export default preview;
